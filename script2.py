#!/usr/bin/env python
import os

lst_explicit = ["explicit_other_multiple_same_awk_command.py","explicit_other_multiple_same_cat_command.py","explicit_other_multiple_same_copy_command.py","explicit_other_multiple_same_diff_command.py","explicit_other_multiple_same_grep_command.py","explicit_other_multiple_same_md5sum_command.py","explicit_other_multiple_same_rsync_command.py"]

cmpts = ["other","noother"]
cmpts2 = ["explicit","public","private"]
cmds =  ["awk","cat","copy","diff","grep","md5sum","rsync"]

for cnt, k in enumerate(cmds):
	for i in cmpts:
		for j in cmpts2:
			if i == "other" and j == "public":
				testName = j + "_" + i + "_multiple_same_" + k + "_command.py"
				cmd1 = "sudo cp  ./" + lst_explicit[cnt]  + "  ./" + testName
				#print cmd1
				os.system(cmd1)
				cmd_1 = "sudo sed -i s/'explicit/public/'" + " ./" + testName
				os.system(cmd_1)
				rep_text = 'setenv.setWorkspace(workspace + str(i), wspDirPath + str(i), "public", None)'
				rep_text_2 = 'setenv.setWorkspace(workspace + str(i), wspDirPath + str(i), "false", None)'
				cmd_1 = "sudo sed -i s/'" + rep_text + "/" + rep_text_2  + "/'  ./" + testName
				os.system(cmd_1)


			if i == "noother" and j == "explicit":
				testName = j + "_" + i + "_multiple_same_" + k + "_command.py"
				cmd2 = "sudo cp  ./" + lst_explicit[cnt] + "  ./" + testName
				os.system(cmd2)
				cmd_2 = "sudo sed -i s/'other = True/other = False/'" + " ./" + testName
				os.system(cmd_2)
				text_rep = j + "_other" + "_multiple_same_" + k + "_command" + '"'
				#print text_rep
				text_rep2 = j + "_" + i + "_multiple_same_" + k + "_command" + '"'	
				cmd_1 = "sudo sed -i s/'" + text_rep + "/" + text_rep2 + "/'  ./" + testName
				os.system(cmd_1)

				text_rep = j + "_other" + "_multiple_same_" + k + "_command"
				text_rep2 = j + "_" + i + "_multiple_same_" + k + "_command"
				cmd_1 = "sudo sed -i s/'" + text_rep + "/" + text_rep2 + "/'  ./" + testName
				#print cmd_1
				os.system(cmd_1)

			if i == "noother" and j == "public":
				testName = j + "_" + i + "_multiple_same_" + k + "_command.py"
				cmd3 = "sudo cp  ./" + lst_explicit[cnt]  + "  ./" + testName
				os.system(cmd3)
				cmd_4 = "sudo sed -i s/'explicit/public/'" + " ./" + testName
				os.system(cmd_4)
				text_rep = j + "_other" + "_multiple_same_" + k + "_command" + '"'
				text_rep2 = j + "_" + i + "_multiple_same_" + k + "_command" + '"'
				cmd_1 = "sudo sed -i s/'" + text_rep + "/" + text_rep2 + "/'  ./" + testName
				os.system(cmd_1)

				cmd_1 = "sudo sed -i s/'explicit/public/'" + " ./" + testName
				os.system(cmd_1)
				cmd_2 = "sudo sed -i s/'other = True/other = False/'" + " ./" + testName
				os.system(cmd_2)
				rep_text = 'setenv.setWorkspace(workspace + str(i), wspDirPath + str(i), "public", None)'
				rep_text_2 = 'setenv.setWorkspace(workspace + str(i), wspDirPath + str(i), "false", None)'
				cmd_1 = "sudo sed -i s/'" + rep_text + "/" + rep_text_2  + "/'  ./" + testName
				os.system(cmd_1)

				text_rep = "usage: " + j + "_other" + "_multiple_same_" + k + "_command"
				text_rep2 = "usage: " + j + "_" + i + "_multiple_same_" + k + "_command"
				cmd_1 = "sudo sed -i s/'" + text_rep + "/" + text_rep2 + "/'  ./" + testName
				#print cmd_1
				os.system(cmd_1)
			if i == "noother" and j == "private":
				testName = j + "_" + i + "_multiple_same_" + k + "_command.py"
				cmd4 = "sudo  cp  ./" +    lst_explicit[cnt] + "  ./" + testName
				os.system(cmd4)
				cmd_4 = "sudo sed -i s/'explicit/private/'" + " ./" + testName
				os.system(cmd_4)
				cmd_4 = "sudo sed -i s/'other = True/other = False/'" + " ./" + testName
				os.system(cmd_4)
				rep_text = 'setenv.setWorkspace(workspace + str(i), wspDirPath + str(i), "private", None)'
				rep_text_2 = 'setenv.setWorkspace(workspace + str(i), wspDirPath + str(i), "true", None)'
				cmd_1 = "sudo sed -i s/'" + rep_text + "/" + rep_text_2  + "/'  ./" + testName
				os.system(cmd_1)
				text_rep = j + "_other" + "_multiple_same_" + k + "_command" + '"'
				text_rep2 = j + "_" + i + "_multiple_same_" + k + "_command" + '"'	
				cmd_1 = "sudo sed -i s/'" + text_rep + "/" + text_rep2 + "/'  ./" + testName
				os.system(cmd_1)
				
				text_rep = "usage: " + j + "_other" + "_multiple_same_" + k + "_command"
				text_rep2 = "usage: " + j + "_" + i + "_multiple_same_" + k + "_command"
				cmd_1 = "sudo sed -i s/'" + text_rep + "/" + text_rep2 + "/'  ./" + testName
				#print cmd_1
				os.system(cmd_1)
			if i == "other" and j == "private":
				testName = j + "_" + i + "_multiple_same_" + k + "_command.py"
				cmd5 = "sudo cp  ./" + lst_explicit[cnt] + "  ./" + testName
				os.system(cmd5)
				cmd_4 = "sudo sed -i s/'explicit/private/'" + " ./" + testName
				os.system(cmd_4)
				rep_text = 'setenv.setWorkspace(workspace + str(i), wspDirPath + str(i), "private", None)'
				rep_text_2 = 'setenv.setWorkspace(workspace + str(i), wspDirPath + str(i), "true", None)'
				cmd_1 = "sudo sed -i s/'" + rep_text + "/" + rep_text_2  + "/'  ./" + testName
				os.system(cmd_1)
				text_rep = j + "_other" + "_multiple_same_" + k + "_command" + '"'
				text_rep2 = j + "_" + i + "_multiple_same_" + k + "_command" + '"'	
				cmd_1 = "sudo sed -i s/'" + text_rep + "/" + text_rep2 + "/'  ./" + testName
				print cmd_1
				os.system(cmd_1)

#sect = " ./private_tr_nocache_noother_to_local_FS/*"
##
#txt1 = "import datetime"
#txt = 'date = datetime.datetime.now().strftime("%H_%M_%S")'
##
#cmd1 = "sed -i '/import getopt/ a\\" + txt1 + "'" + sect
#os.system(cmd1)
#cmd2 = "sed -i '/" + txt1 + "/ a\\" + txt + "'" +  sect
#os.system(cmd2)
#
##
##
##
#wsRep1 = ' workspace1 = "FileSystem_" + setenv.getMachineVersion() + "_" + setenv.getBitness() + "_" + setenv.getUsername()'
#wsRep2 = 'workspace2 = testEnvPriv + "_" + setenv.getMachineVersion() + "_" + setenv.getBitness() + "_" + setenv.getUsername()'
#wsRep3 = 'workspaceT = testEnvPriv + "_" + setenv.getMachineVersion() + "_" + setenv.getBitness() + "_" + setenv.getUsername() + "_T"'
#
#cmd3 = "sed -i s/'" + wsRep1 + "/" + wsRep1 +  ' + "_"' + " + date/' " + sect
#os.system(cmd3)
#
#cmd4 = "sed -i s/'" + wsRep2 + "/" + wsRep2 +  ' + "_"' + " + date/' " + sect



